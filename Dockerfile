FROM gromacs/cmake-3.13.0-llvm-8-tsan-master
MAINTAINER Terézia Slanináková, xslanin@mail.muni.cz

ENV DEBIAN_FRONTEND=noninteractive 
ENV TZ=Europe/Prague

RUN apt update && apt install wget unzip python3 -y
RUN pip install --upgrade pip
RUN pip install scikit-learn
RUN pip install pandas

ADD . /home/user
WORKDIR /home/user

RUN useradd -m -u 1000 user
RUN chown -R 1000:1000 /home/user

RUN python3 /home/user/generate-gaussian-data.py
RUN mkdir /home/user/outputFiles-gauss10m8d-K_150
RUN chown -R 1000:1000 /home/user/outputFiles-gauss10m8d-K_150
RUN make
# ./main 1 150 8 1 1 150 1 >> outputFiles-gauss10m8d-K_150/output.txt 2>&1
#CMD ["/bin/sh", "-c", "./main 1 150 8 1 1 150 1 >> outputFiles-gauss10m8d-K_150/output.txt 2>&1"]
CMD ["/bin/sh", "-c", "./main 1 2 8 1 1 2 1"]
