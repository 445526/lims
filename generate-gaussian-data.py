from sklearn.datasets import make_blobs
from sklearn.preprocessing import MinMaxScaler
import pandas as pd

#N_SAMPLES = 10000000
N_SAMPLES = 10000

if __name__ == '__main__':
  X,_ = make_blobs(n_samples=N_SAMPLES, n_features=8, centers=150, cluster_std=0.05, center_box=(0,1), random_state=0)
  pd.DataFrame(MinMaxScaler().fit_transform(X)).to_csv('gauss-10m-8d.txt', header=None, index=None)